﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Domain.SSO.Client
{
    public class WebConfig
    {

        public static readonly string sso_url = ConfigurationManager.AppSettings["sso.url"];

    }
}
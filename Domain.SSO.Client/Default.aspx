﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Domain.SSO.Client.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>服务提供者 - 第三方应用</title>
</head>
<body>
    <form id="form1" runat="server">
        <h1>服务提供者 - 第三方应用</h1>
        <p>
            <ul>
                <li><asp:Label runat="server" ID="lblMessage"></asp:Label></li>
                <li><asp:HyperLink runat="server" ID="keeptoken" Visible="False" Text="续订"></asp:HyperLink></li>
                <li><asp:HyperLink runat="server" ID="logout" Visible="False" Text="退出"></asp:HyperLink></li>
            </ul>
            
        </p>
    </form>
</body>
</html>

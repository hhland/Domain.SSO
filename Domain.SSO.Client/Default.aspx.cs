﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Windows.Forms;
using Domain.SSO.Client.AuthTokenService;


namespace Domain.SSO.Client
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string token = Request.QueryString["token"];
            if (string.IsNullOrWhiteSpace(token))
            {
                if (Session["token"] != null) token = Session["token"].ToString();
            }

            string action = Request.QueryString["action"];
            AuthTokenService.AuthTokenServiceSoapClient authTokenService = new AuthTokenService.AuthTokenServiceSoapClient();

            if (action == "keeptoken")
            {
                string msg = "续订成功";
                if (!authTokenService.KeepToken(token))
                {
                    msg = "续订失败";
                }
                string url = "Default.aspx?token=" + token;
                Response.Write("<script>alert('"+msg+"');window.location.href='"+url+"';</script>");
                
                return;
            }

           /* 
            if (!string.IsNullOrEmpty(Request["token"]))
            {
                tokenID = Request["token"];
                HttpCookie tokenCookie = new HttpCookie("token", tokenID);
                tokenCookie.HttpOnly = true;
                Response.Cookies.Set(tokenCookie);
            }
            else if (Request.Cookies["token"] != null)
            {
                tokenID = Request.Cookies["token"].Value;
                Console.Write("123213123123");
            }
           */
   
            SSOToken ssoToken= authTokenService.ValidateToken(token);


            if (ssoToken!=null)
            {
                Session["token"] = token;
                this.lblMessage.Text = string.Format("登录成功<br/>LoginID:{0}<br/>ID:{1}<br/>UserName:{2}<br/>AuthTime:{3}<br/>TimeOut:{4}"
                        ,
                        ssoToken.LoginID
                        ,ssoToken.ID
                        ,ssoToken.User.UserName
                        ,ssoToken.AuthTime.ToLongDateString()+" "+ssoToken.AuthTime.ToLongTimeString()
                        ,ssoToken.TimeOut
                        );
                keeptoken.Visible = logout.Visible = true;
                logout.NavigateUrl = string.Format(WebConfig.sso_url + "/logout.aspx?returnUrl=http://{0}:{1}/Default.aspx"
                    ,Request.Url.Host
                    ,Request.Url.Port

                    );
                                     
                keeptoken.NavigateUrl = "Default.aspx?action=keeptoken&token=" + token;
            }
            else
            {
                Response.Write("<script>alert('您要先登录可访问')</script>");
                this.lblMessage.Text = string.Format("未登录 <a href='{0}/Login.aspx?returnUrl={1}'>登录</a>"
                    ,WebConfig.sso_url
                    ,Request.Url.ToString()
                    );

               
            }


        }
/*
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "", "alert('OK');", true);
            CookieContainer cc = new CookieContainer();
            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create("http://sso-server.com/login.aspx");//url为访问的地址
            wr.CookieContainer = cc;
            Stream strm = wr.GetResponse().GetResponseStream();
        }
*/
    }
}
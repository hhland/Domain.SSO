﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Domain.SSO.Server
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            /*
            HttpCookie cookie = new HttpCookie("MyCook");//初使化并设置Cookie的名称
            cookie.Values.Add("token", "111");
            Response.AppendCookie(cookie);
            */

            string returnUrl = Request.Params["returnUrl"];
            if (Session["holder"] == null)
            {
                Session["holder"] = 1;
            }

            if (Domain.Security.SmartAuthenticate.LoginUser != null)
            {
                info.Text = "登录成功，登录用户：" + Domain.Security.SmartAuthenticate.LoginUser.UserName;
                logout.Text = "退出";
                logout.NavigateUrl = "logout.aspx";
         
                toclient.Text = "点此继续访问服务提供者";
                toclient.NavigateUrl = "SSO.aspx?returnUrl=" + returnUrl;
                
            }
            else
            {
                info.Text = "未登录 <a href='login.aspx'>登录</a>";
            }
        }

    
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Management;
using System.Net;

namespace Domain.SSO.Server
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string getIP()
        {
            string strHostName = Dns.GetHostName();//得到本机的主机名
            IPHostEntry ipEntry = Dns.GetHostByName(strHostName);//取得本机IP
            string strAddr = ipEntry.AddressList[0].ToString();
            return strAddr;

        }

        public string getMac()//获取本机mac地址
        {
            string MacAddr = null;
            ManagementObjectSearcher Query = new ManagementObjectSearcher("select * from Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection QueryCollection = Query.Get();
            foreach (ManagementObject mo in QueryCollection)
            {
                if(mo["IPEnabled"].ToString()== "True")
                     MacAddr = mo["MacAddress"].ToString();
            }
            return MacAddr;
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string userName = this.txtUserName.Text.Trim();
            string password = this.txtPassword.Text.Trim();

            if (string.IsNullOrEmpty(userName))
            {
                this.lblMessage.Text = "用户名不能为空";
                return;
            }

            if (string.IsNullOrEmpty(password))
            {
                this.lblMessage.Text = "密码不能为空";
                return;
            }

            if (Domain.Security.SmartAuthenticate.AuthenticateUser(userName, password, true))
            {
                //
               
                //
                /*this.lblMessage.Text = "登录成功";
                HttpCookie tokenCookie = new HttpCookie("token", "111");
                tokenCookie.HttpOnly = true;
                Response.Cookies.Set(tokenCookie);*/
                string myMac = getMac();
                string myIP = getIP();
                
                string returnUrl = Request.QueryString["returnUrl"];
                if (string.IsNullOrEmpty(returnUrl))
                {
                    returnUrl = "default.aspx";
                }
                string url = string.Format("Default.aspx?returnUrl={0}",returnUrl);
                Response.Redirect(url);
                //Response.Redirect("default.aspx");
                //else
                //    Response.Redirect(returnUrl);
            }
            else
            {
                this.lblMessage.Text = "登录失败，请重试";
                return;
            }
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Domain.SSO.Server.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Domain.SSO.Server</title>
</head>
<body>
    <form id="form1" runat="server">
        <h1>身份提供者</h1>
        <p>
            <asp:Label runat="server" ID="info"></asp:Label>
            <ol>
                <li>
                    <asp:HyperLink ID="logout" runat="server"  ></asp:HyperLink>
                </li>
                 <li>
                    <asp:HyperLink ID="toclient" runat="server"></asp:HyperLink>
                </li>
                 
            </ol> 
           
        </p>
    </form>
</body>
</html>

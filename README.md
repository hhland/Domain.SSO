#Domain.SSO

1. 项目类型：一个web项目，实现语言优先用C#和Java
2. 毕设题目：一键登录在网络身份认证中的风险与改进
3. 协议：OpenID协议（实现统一认证功能）与OAuth协议（实现用户授权）
OpenID协议流程：


需求：实现一键登录流程、针对钓鱼攻击和重放攻击做出改进（两个可展示的功能）。





描述：登录页面有【登录】和【注册】（不用实现注册功能）。用户登录服务器后，要进一步访问第三方服务者时，如果未登录，要提示【登录】，另有【使用已有账号登录】的选项，登录后要让用户做出【同意授权访问】的选择，其中【同意授权访问】这个页面写出相关条款：①此网站可以访问您的个人信息②您在此网站的评论可以发布到您的空间上③此网站可以记录您的访问。登录成功的页面简洁即可。第三方服务者 要设置 两三个。

改进：①防钓鱼攻击的改进是在认证过程中，加密url，加密算法选简单的就行，要能展示加密的过程。
          ②防重放攻击的改进是，在认证过程中，加一个步骤，记录本机的MAC地址，点击登录第三方服务者的时候检测本机的MAC地址，与刚登陆的时候的MAC是否相同。